from flask import Flask, render_template, request
import clips

app = Flask(__name__)

env = clips.Environment()
env.load('horror-movies.clp')
env.reset()
env.run()

msg_template = env.find_template('message')

message = dict(list(msg_template.facts())[0])
layout = {
    'name': message['name'],
    'question': message['question'],
    'answers': message['answers'],
    'selected': ''
}

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if 'Submit' in request.form:
            name = request.form['name']
            selected_value = request.form['answer']
            print(name, selected_value)
            
            if selected_value:
                value = selected_value
                env.assert_string('({} "{}")'.format(name, value))
                env.run()

                message_list = list(msg_template.facts())

                if message_list:
                    message = dict(message_list[0])
                    layout['name'] = message['name']
                    layout['question'] = message['question']
                    layout['answers'] = message['answers']
                else:
                    layout['question'] = 'I already know what you should watch:'
                    layout['answers'] = []

                    movie = env.find_template('movie')
                    selected = dict(list(movie.facts())[0])
                    layout['selected'] = 'You should watch ' + selected['title']
        elif 'Restart' in request.form:
            env.reset()
            env.run()

            message_list = list(msg_template.facts())
            message = dict(message_list[0])
            layout['name'] = message['name']
            layout['question'] = message['question']
            layout['answers'] = message['answers']
            layout['selected'] = ''

    return render_template('index.html', layout=layout)

if __name__ == '__main__':
    app.run(debug=True)
