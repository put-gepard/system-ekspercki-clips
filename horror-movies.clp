(deftemplate message
    (slot name)
    (slot question)
    (multislot answers)
)

(deftemplate movie
    (slot title)
)

(defrule age
    =>
    (assert 
        (message 
            (name "age")
            (question "How old are you?") 
            (answers "Under 18" "Over 18")
        )
    )
)

(defrule gender
    ?f <- (age "Under 18")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "gender")
            (question "Are you male or female?")
            (answers "Female" "Male")
        )
    )   
)

(defrule twilight-result
    ?f <- (gender "Female")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Twilight")))
)

(defrule catholic
    ?f <- (gender "Male")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "catholic")
            (question "Are you Catholic?")
            (answers "Yes" "No")
        )
    )   
)

(defrule killer-nun-result
    ?f <- (catholic "Yes")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Killer Nun")))
)

(defrule slaughter-hotel-result
    ?f <- (catholic "No")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Slaughter Hotel")))
)

(defrule dead-or-living
    ?f <- (age "Over 18")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "dead-or-living")
            (question "Are you more frightened by the dead or the living?")
            (answers 
            "Have you seen the news lately? The living, no matter what planet they're from."
            "Things that crawl scare me more than anything else."
            "I don't care as long as the devil isn't involved."
            "I'm frightened of the dead."
            "I'm most afraid of anything that's a little bit of both.")
        )
    )   
)

(defrule legs
    ?f <- (dead-or-living "Things that crawl scare me more than anything else.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "legs")
            (question "How many legs scare you the most?")
            (answers "Eight" "Six" "Other")
        )
    )   
)

(defrule them-result
    ?f <- (legs "Six")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Them!")))
)

(defrule human-centipede-result
    ?f <- (legs "Other")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "The Human Centipede")))
)

(defrule cast
    ?f <- (legs "Eight")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "cast")
            (question "I prefer films starring members of...")
            (answers "The cast of Star Trek" "The cast of Roseanne")
        )
    )   
)

(defrule arachnophobia-result
    ?f <- (cast "The cast of Roseanne")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Arachnophobia")))
)

(defrule kingdom-of-the-spiders-result
    ?f <- (cast "The cast of Star Trek")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Kingdom of the Spiders")))
)

(defrule children
    ?f <- (dead-or-living  "I don't care as long as the devil isn't involved.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "children")
            (question "Do you like children?")
            (answers "No" "I love the cute little buggers! Especially when they're...")
        )
    )   
)

(defrule haxan-result
    ?f <- (children "No")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Haxan")))
)

(defrule children2
    ?f <- (children  "I love the cute little buggers! Especially when they're...")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "children2")
            (question "Especially when the're")
            (answers "babies" "in grade school.")
        )
    )   
)

(defrule rosemarys-baby-result
    ?f <- (children2 "babies")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Rosemary's baby")))
)

(defrule pea-soup
    ?f <- (children2  "in grade school.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "pea-soup")
            (question "Do you ever want to eat pea soup again?")
            (answers "Who cares?" "Yes, I love pea soup")
        )
    )   
)

(defrule the-omen-result
    ?f <- (pea-soup "Yes, I love pea soup")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "The Omen")))
)

(defrule the-exorcist-result
    ?f <- (pea-soup "Who cares?")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "The Exorcist")))
)

(defrule body
    ?f <- (dead-or-living  "I'm frightened of the dead.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "body")
            (question "Do you care if they have a body?")
            (answers "Without a body is scarier." 
            "Yes. Bodies are how they eat your brains.")
        )
    )   
)

(defrule george
    ?f <- (body  "Without a body is scarier.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "george")
            (question "Could George C. Scott protect you?")
            (answers "No" "Yes")
        )
    )   
)

(defrule poltergeist-result
    ?f <- (george "No")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Poltergeist")))
)

(defrule the-changeling-result
    ?f <- (george "Yes")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "The Changeling")))
)

(defrule doggo
    ?f <- (body  "Yes. Bodies are how they eat your brains.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "doggo")
            (question "Can dogs look up?")
            (answers "Yes" "What's all this talk of dogs? Let's go to the mall.")
        )
    )   
)

(defrule shaun-of-the-dead-result
    ?f <- (doggo "Yes")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Shaun of the Dead")))
)

(defrule dawn-of-the-dead-result
    ?f <- (doggo "What's all this talk of dogs? Let's go to the mall.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Dawn of the Dead")))
)

(defrule christopher-lee
    ?f <- (dead-or-living  "I'm most afraid of anything that's a little bit of both.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "christopher-lee")
            (question "Christopher Lee is...")
            (answers "that guy who played Count Dooku" "a legend")
        )
    )   
)

(defrule hippies
    ?f <- (christopher-lee "a legend")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "hippies")
            (question "Do you like hippies?")
            (answers "No" "The real terror is 19th century virginal women")
        )
    )   
)

(defrule dracula-ad-1972-result
    ?f <- (hippies "No")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Dracula A.D. 1972")))
)

(defrule the-horror-of-dracula-result
    ?f <- (hippies "The real terror is 19th century virginal women")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "The Horror of Dracula")))
)

(defrule vampires
    ?f <- (christopher-lee "that guy who played Count Dooku")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "vampires")
            (question "I prefer my vampires...")
            (answers "silent" "Eastern European" "with a big beehive hairdo")
        )
    )   
)

(defrule nosferatu-result
    ?f <- (vampires "silent")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Nosferatu")))
)

(defrule dracula-1931-result
    ?f <- (vampires "Eastern European")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Dracula (1931)")))
)

(defrule dracula-1992-result
    ?f <- (vampires "with a big beehive hairdo")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Dracula (1992)")))
)

(defrule hair
    ?f <- (dead-or-living "Have you seen the news lately? The living, no matter what planet they're from.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "hair")
            (question "What are your feelings about dogs, hairy people, or hairy dogs?")
            (answers "I poop my pants every time I smell Alpo." 
            "Dogs and/or bearded guys are cool with me.")
        )
    )   
)

(defrule environmentalist
    ?f <- (hair "I poop my pants every time I smell Alpo.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "environmentalist")
            (question "Are you an environmentalist?")
            (answers "Every day is Earth Day." 
            "Meh. I do my part but don't go nuts.")
        )
    )   
)

(defrule wolfen-result
    ?f <- (environmentalist "Every day is Earth Day.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Wolfen")))
)

(defrule puberty
    ?f <- (environmentalist "Meh. I do my part but don't go nuts.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "puberty")
            (question "Did puberty suck?")
            (answers "Yes" "It wasn't so bad")
        )
    )   
)

(defrule ginger-snaps-result
    ?f <- (puberty "Yes")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Ginger Snaps")))
)

(defrule visit-the-uk
    ?f <- (puberty "It wasn't so bad")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "visit-the-uk")
            (question "If visiting th UK, which would you rather do?")
            (answers "Picadilly Circus, maybe an adult movie theater" 
            "Visit a countryside...")
        )
    )   
)

(defrule an-american-werewolf-in-london-result
    ?f <- (visit-the-uk "Picadilly Circus, maybe an adult movie theater")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "An American werewolf in London")))
)

(defrule countryside
    ?f <- (visit-the-uk "Visit a countryside...")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "countryside")
            (question "Visit a countryside...")
            (answers "castle." "battlefield.")
        )
    )   
)

(defrule the-wolf-man-result
    ?f <- (countryside "castle.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "The Wolf Man")))
)

(defrule dog-soldiers-result
    ?f <- (countryside "battlefield.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Dog Soldiers")))
)

(defrule intelligence
    ?f <- (hair "Dogs and/or bearded guys are cool with me.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "intelligence")
            (question "Are you worried about people with higher intelligence?")
            (answers "Not really. Murderous maniacs, on the other hand..."
            "Definitely. Especially when they're...")
        )
    )   
)

(defrule maniacs
    ?f <- (intelligence "Not really. Murderous maniacs, on the other hand...")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "maniacs")
            (question "Specifically I'm most worried...")
            (answers 
            "of people in sweathers"
            "of people who take Halloween too seriously"
            "of overzealous hockey fans"
            "about Texas"
            "of movie nerds"
            "of vicious androgynous children")
        )
    )   
)

(defrule a-nightmare-on-elm-street-result
    ?f <- (maniacs "of people who take Halloween too seriously")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "A Nightmare on Elm Street")))
)

(defrule halloween-result
    ?f <- (maniacs "of people who take Halloween too seriously")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Halloween")))
)

(defrule friday-the-13th-result
    ?f <- (maniacs "of overzealous hockey fans")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Friday the 13th")))
)

(defrule the-texas-chainsaw-massacre-result
    ?f <- (maniacs "about Texas")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "The Texas Chainsaw Massacre")))
)

(defrule scream-result
    ?f <- (maniacs "of movie nerds")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Scream")))
)

(defrule sleepaway-camp-result
    ?f <- (maniacs "of vicious androgynous children")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Sleepaway Camp")))
)

(defrule planet-or-lab
    ?f <- (intelligence "Definitely. Especially when they're...")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "planet-or-lab")
            (question "Especially when they're...")
            (answers "from another planet" "working in the lab")
        )
    )   
)

(defrule circus
    ?f <- (planet-or-lab "from another planet")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "circus")
            (question "Does the circus scare you?")
            (answers "Where clowns live? Are you crazy? No." "Not really")
        )
    )   
)

(defrule killer-klowns-from-outer-space-result
    ?f <- (circus "Where clowns live? Are you crazy? No.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Killer Klowns from Outer Space")))
)

(defrule alien-result
    ?f <- (circus "Not really")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Alien")))
)

(defrule insects
    ?f <- (planet-or-lab "working in the lab")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "insects")
            (question "Are you also afraid of insects?")
            (answers "I hate them." "Things I swat with my hand? No.")
        )
    )   
)

(defrule the-fly-result
    ?f <- (insects "I hate them.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "The Fly")))
)

(defrule re-animated-head
    ?f <- (insects "Things I swat with my hand? No.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert 
        (message 
            (name "re-animated-head")
            (question "How do you feel about people having sex with severed re-animated heads??")
            (answers "I love seeing people do that thing you just said!" 
            "We're talking about corpses. Show a little decorum.")
        )
    )   
)

(defrule re-animator-result
    ?f <- (re-animated-head "I love seeing people do that thing you just said!")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Re-animator")))
)

(defrule frankenstein-result
    ?f <- (re-animated-head "We're talking about corpses. Show a little decorum.")
    ?id <- (message (name ?x))
    =>
    (retract ?id)
    (retract ?f)
    (assert (movie (title "Frankenstein")))
)
