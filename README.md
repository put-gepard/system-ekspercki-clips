# Horror Recommender

Aplikacja przeglądarkowa wykorzystująca system ekspercki w Clips

# Instalacja

```
python3 -m venv venv
```

```
source ./venv/bin/activate
```

```
pip3 install -requirement requirements.txt
```

```
python3 main.py
```

Otworzy się deweloperski serwer http na porcie 5000

# Autorzy:

Katarzyna Jabłońska 151761

Tymoteusz Jagła 151811
